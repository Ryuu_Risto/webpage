let corner2 = new p5(p => {
	let canvas;
	let max = 150;

	p.setup = () => {
		canvas = p.createCanvas(max, max);
		canvas.parent("corner2");
		p.strokeWeight(0);

		p.fill("#ff0000");
		p.beginShape();
			p.vertex(max, max - max/2);
			p.vertex(max - max/2, max - max/2);
			p.vertex(max - max/2, max - max);
			p.vertex(max - max/4*3, max - max/4*3);
			p.vertex(max - max/4*3, max - max/4);
			p.vertex(max - max/4, max - max/4)
		p.endShape(p.CLOSE);

		p.fill("#ffff00");
		p.beginShape();
			p.vertex(max, max - max/2);
			p.vertex(max - max/2, max - max/2);
			p.vertex(max - max/2, max - max);
			p.vertex(max - max/12*8, max - max/12*10);
			p.vertex(max - max/12*8, max - max/12*4);
			p.vertex(max - max/12*2, max - max/12*4)
		p.endShape(p.CLOSE);

		p.fill("#00ff00");
		p.beginShape();
			p.vertex(max, max - max/2);
			p.vertex(max - max/2, max - max/2);
			p.vertex(max - max/2, max - max);
			p.vertex(max - max/12*7, max - max/12*11);
			p.vertex(max - max/12*7, max - max/12*5);
			p.vertex(max - max/12, max - max/12*5)
		p.endShape(p.CLOSE);
	}
});