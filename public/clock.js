let clock = new p5(p => {
	let canvas;
	let font;
	let max = 150;
	p.preload = () => {
		font = p.loadFont("https://ryuu_risto.gitlab.io/webpage/Roboto-Light.ttf");
		if (typeof bigClock !== 'undefined') max = 1000;
	}

	p.setup = () => {
		canvas = p.createCanvas(max, max, p.WEBGL);
		canvas.parent("clock");
		p.translate(max/2, max/2)
		p.textFont(font);
		p.textSize(max/8);
		p.textAlign(p.RIGHT, p.BASELINE);
	}

	p.draw = () => {
		let d = new Date();
		p.background("#161616");
		let time = d.getTime();
		p.rotateX(time / 1000);
		p.rotateZ(time / 1234);
		p.strokeWeight(5);
		for (let i = -3; i < 9; i++) {
			p.stroke(192)
			if (i == -3) p.stroke("#ff0000");
			let x = max/3 * p.cos(p.PI/6 * i);
			let y = max/3 * p.sin(p.PI/6 * i);
			p.point(x, y);
		}
		p.strokeWeight(1);
		p.stroke("#ff0000");
		p.line(0, 0, max/4 * p.cos(p.PI/30 * d.getSeconds()- p.PI/2), max/4 * p.sin(p.PI/30 * d.getSeconds() - p.PI/2));
		p.stroke("#ffff00");
		p.text("zegar", 0, 0);
		p.line(0, 0, max/5 * p.cos(p.PI/30 * d.getMinutes()- p.PI/2), max/5 * p.sin(p.PI/30 * d.getMinutes()- p.PI/2));
		p.stroke("#00ff00");
		p.line(0, 0, max/6 * p.cos(p.PI/6 * d.getHours()- p.PI/2), max/6 * p.sin(p.PI/6 * d.getHours()- p.PI/2));
	}
});