let corner = new p5(p => {
	let canvas;
	let max = 150;

	p.setup = () => {
		canvas = p.createCanvas(max, max);
		canvas.parent("corner");
		p.strokeWeight(0);

		p.fill("#ff0000");
		p.beginShape();
			p.vertex(0, max);
			p.vertex(max, max/12);
			p.vertex(max, max/6*p.sqrt(2));
		p.endShape(p.CLOSE);

		p.fill("#ffff00");
		p.beginShape();
			p.vertex(0, max);
			p.vertex(max, max/12);
			p.vertex(max, 0);
			p.vertex(max/12*11, 0);
		p.endShape(p.CLOSE);

		p.fill("#00ff00");
		p.beginShape();
			p.vertex(0, max);
			p.vertex(max/12*11, 0);
			p.vertex(max - max/6*p.sqrt(2), 0);
		p.endShape(p.CLOSE);
	}
});